package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import com.google.gson.JsonObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class User {
    public int id;
    public String type;
    public String phid;

    // Fields
    public String username;
    public String realName;
    public List<String> roles = new ArrayList<>();
    public int dateCreated;
    public int dateModified;

    // Policy
    public String policyView;
    public String policyEdit;

    // Availability
    public String availabilityValue;
    public int availabilityUntil;
    public String availabilityName;
    public String availabilityColorRaw;
    public Color availabilityColor;
    public String availabilityEventPHID;

    public User(JsonObject obj) {
        obj = obj.get("result").getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject();
        JsonObject subObj = new JsonObject();
        JsonObject attachmentsObj = obj.get("attachments").getAsJsonObject();
        JsonObject availabilityObj = attachmentsObj.get("availability").getAsJsonObject();

        this.id = obj.get("id").getAsInt();
        this.type = obj.get("type").getAsString();
        this.phid = obj.get("phid").getAsString();

        obj = obj.get("fields").getAsJsonObject();
        this.username = obj.get("username").getAsString();
        this.realName = obj.get("realName").getAsString();

        obj.get("roles").getAsJsonArray().forEach(el -> this.roles.add(el.getAsString()));

        this.dateCreated = obj.get("dateCreated").getAsInt();
        this.dateModified = obj.get("dateModified").getAsInt();

        subObj = obj.get("policy").getAsJsonObject();

        this.policyView = subObj.get("view").getAsString();
        this.policyEdit = subObj.get("edit").getAsString();

        this.availabilityValue = availabilityObj.get("value").getAsString();
        if (!availabilityObj.get("until").isJsonNull()) this.availabilityUntil = availabilityObj.get("until").getAsInt();
        this.availabilityName = availabilityObj.get("name").getAsString();
        this.availabilityColorRaw = availabilityObj.get("color").getAsString();
        this.availabilityColor = Color.getColor(this.availabilityColorRaw);
        if (!availabilityObj.get("eventPHID").isJsonNull()) this.availabilityEventPHID = availabilityObj.get("eventPHID").getAsString();
    }
}
