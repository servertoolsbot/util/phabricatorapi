package com.gitlab.servertoolsbot.util.phabricatorapi.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Web {
    private static Logger logger = LogManager.getLogger(Web.class);

    public static final MediaType TYPE = MediaType.get("application/x-www-form-urlencoded");

    public static String postAndGet(String url, String param) {
        logger.log(Level.DEBUG, "[Conduit API] Making call to " + url + " with parameters " + param);

        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(TYPE, param);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static JsonObject postAndGetAsJson(String url, String param) {
        String returnedJson = postAndGet(url, param);
        logger.log(Level.DEBUG, "[Conduit API] Returned JSON: " + returnedJson);
        return JsonParser.parseString(returnedJson).getAsJsonObject();
    }
}
