package com.gitlab.servertoolsbot.util.phabricatorapi;

public class PhabricatorApi {
    private String url;
    private String urlRaw;
    private String param;
    private String token;
    private static PhabricatorApi papi;

    public PhabricatorApi(String url, String token) {
        if (url == null) throw new IllegalArgumentException("The provided Phabricator url was null");
        if (token == null) throw new IllegalArgumentException("The provided API token was null");
        this.urlRaw = url + (!url.endsWith("/") ? "/" : "");
        this.url = url + (!url.endsWith("/") ? "/" : "") + "api/";
        this.token = token;
        getParam();

        papi = this;
    }
    public static PhabricatorApi getPapi() {
        return papi;
    }

    public String getParam() {
        this.param = "api.token="+token+"&";
        return this.param;
    }

    public String getUrl() {
        return this.url;
    }
    public String getUrl(String method) {
        return this.url + method;
    }
    public String getUrlRaw() {
        return this.urlRaw;
    }
    public String getToken() {
        return this.token;
    }
}
