package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class Project {
    public int id;
    public String type;
    public String phid;

    // Fields
    public String name;
    public String slug;

    public String subtype;
    public String milestone;
    public int depth;
    public int parentId;
    public String parentPhid;
    public String parentName;

    // Icon
    public String iconKey;
    public String iconName;
    public String iconIcon;

    // Color
    public String colorKey;
    public String colorName;
    public Color color;

    // TODO: SPACE
    public String spacePHID;

    public int dateCreated;
    public int dateModified;


    // Policy
    public String policyView;
    public String policyEdit;
    public String policyJoin;

    public String description;


    // Attachments
    // Members
    public List<String> memberPHIDS = new ArrayList<>();
    public Optional<List<User>> members() {
        return memberPHIDS.size() == 0 ? Optional.empty() : Optional.of(memberPHIDS.stream().map(phid -> {
            HashMap<String, Object> constraints = new HashMap<>();
            constraints.put("phids", Arrays.asList(phid));

            UserSearch search = new UserSearch();
            search.setConstraints(constraints);
            search.setLimit(1);

            return search.perform();
        }).collect(Collectors.toList()));
    }

    public List<String> watcherPHIDs = new ArrayList<>();

    public Optional<List<User>> watchers() {
        return watcherPHIDs.size() == 0 ? Optional.empty() : Optional.of(watcherPHIDs.stream().map(phid -> {
            HashMap<String, Object> constraints = new HashMap<>();
            constraints.put("phids", Arrays.asList(phid));

            UserSearch search = new UserSearch();
            search.setConstraints(constraints);
            search.setLimit(1);

            return search.perform();
        }).collect(Collectors.toList()));
    }

    public List<String> ancestorPHIDs = new ArrayList<>();

    public Optional<List<Project>> ancestors() {
        return ancestorPHIDs == null ? Optional.empty() : Optional.of(ancestorPHIDs.stream().map(phid -> {
            HashMap<String, Object> constraints = new HashMap<>();
            constraints.put("phids", Arrays.asList(phid));

            ProjectSearch search = new ProjectSearch();
            search.setConstraints(constraints);
            search.setLimit(1);

            return search.perform();
        }).collect(Collectors.toList()));
    }

    public Project(JsonObject obj) {
        obj = obj.get("result").getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject();
        JsonObject attachments = obj.get("attachments").getAsJsonObject();
        JsonObject temp = new JsonObject();

        this.id = obj.get("id").getAsInt();
        this.type = obj.get("type").getAsString();
        this.phid = obj.get("phid").getAsString();

        obj = obj.get("fields").getAsJsonObject();
        this.name = obj.get("name").getAsString();
        this.slug = obj.get("slug").getAsString();
        this.subtype = obj.get("subtype").getAsString();
        if (!obj.get("milestone").isJsonNull()) this.milestone = obj.get("milestone").getAsString();
        this.depth = obj.get("depth").getAsInt();
        if (!obj.get("parent").isJsonNull()) {
            temp = obj.get("parent").getAsJsonObject();
            this.parentId = temp.get("id").getAsInt();
            this.parentPhid = temp.get("phid").getAsString();
            this.parentName = temp.get("name").getAsString();
        }

        temp = obj.get("icon").getAsJsonObject();
        this.iconKey = temp.get("key").getAsString();
        this.iconName = temp.get("name").getAsString();
        this.iconIcon = temp.get("icon").getAsString();

        temp = obj.get("color").getAsJsonObject();
        this.colorKey = temp.get("key").getAsString();
        if (!this.colorKey.equalsIgnoreCase("disabled")) {
            this.colorName = temp.get("name").getAsString();
            this.color = Color.getColor(this.name);
        }

        if (!obj.get("spacePHID").isJsonNull()) this.spacePHID = obj.get("spacePHID").getAsString();
        this.dateCreated = obj.get("dateCreated").getAsInt();
        this.dateModified = obj.get("dateModified").getAsInt();

        temp = obj.get("policy").getAsJsonObject();
        this.policyView = temp.get("view").getAsString();
        this.policyEdit = temp.get("edit").getAsString();
        this.policyJoin = temp.get("join").getAsString();

        this.description = obj.get("description").getAsString();

        System.out.println(new Gson().toJson(attachments));
        if (!attachments.get("members").getAsJsonObject().get("members").isJsonNull()) attachments.get("members").getAsJsonObject().get("members").getAsJsonArray().forEach(el -> this.memberPHIDS.add(el.getAsJsonObject().get("phid").getAsString()));
        if (!attachments.get("watchers").getAsJsonObject().get("watchers").isJsonNull()) attachments.get("watchers").getAsJsonObject().get("watchers").getAsJsonArray().forEach(el -> this.watcherPHIDs.add(el.getAsJsonObject().get("phid").getAsString()));
        if (!attachments.get("ancestors").getAsJsonObject().get("ancestors").isJsonNull()) attachments.get("ancestors").getAsJsonObject().get("ancestors").getAsJsonArray().forEach(el -> this.ancestorPHIDs.add(el.getAsJsonObject().get("phid").getAsString()));
    }
}
