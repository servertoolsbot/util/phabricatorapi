package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import java.util.Arrays;

public class ProjectSearch extends ApplicationSearch {
    public ProjectSearch() {
        this.setMethod("project.search");
        this.setAttachments(Arrays.asList("members", "watchers", "ancestors"));
    }

    public Project perform() {
        return new Project(this.performRequest());
    }
}
