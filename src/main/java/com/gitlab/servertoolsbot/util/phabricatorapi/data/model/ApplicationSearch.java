package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.util.phabricatorapi.util.Web;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

abstract class ApplicationSearch {
    protected String method = "";

    protected String queryKey = "all";
    protected HashMap<String, Object> constraints = new HashMap<>();
    protected List<String> attachments = new ArrayList<>();
    protected List<String> order = new ArrayList<>();
    protected int after;
    protected int before;
    protected int limit = 100;


    public JsonObject performRequest() {
        return Web.postAndGetAsJson(PhabricatorApi.getPapi().getUrl(this.getMethod()), this.parseParam());
    }

    public String parseParam() {
        AtomicReference<String> parsed = new AtomicReference<>();
        parsed.set("");

        parsed.set("api.token" + "=" + PhabricatorApi.getPapi().getToken() + "&");
        parsed.set(parsed.get() + "queryKey" + "=" + this.getQueryKey() + "&");
        this.getConstraints().forEach((key, value) -> {
            if (value instanceof List) {
                for (int i = 0; i < ((List) value).size(); i++) {
                    parsed.set(parsed.get() + "constraints" + ("[" + key + "]") + ("[" + i + "]") + "=" + ((List)value).get(i) + "&");
                }
            } else {
                parsed.set(parsed.get() + "constraints" + ("[" + key + "]") + "=" + value + "&");
            }
        });
        for (int i = 0; i < this.getAttachments().size(); i++) {
            parsed.set(parsed.get() + "attachments" + ("[" + this.getAttachments().get(i) + "]") + "=" + "1" + "&");
        }
        for (int i = 0; i < this.getOrder().size(); i++) {
            parsed.set(parsed.get() + "order" + (this.getOrder().size() > 1 ? "[" + i + "]" : "") + "=" + this.getOrder().get(i) + "&");
        }
        if (this.getAfter() != 0) parsed.set(parsed.get() + "after" + "=" + this.getAfter() + "&");
        if (this.getBefore() != 0) parsed.set(parsed.get() + "before" + "=" + this.getBefore() + "&");
        parsed.set(parsed.get() + "limit" + "=" + this.getLimit() + "&");

        return parsed.get();
    }


    public String getMethod() {
        return this.method;
    }
    protected void setMethod(String method) {
        this.method = method;
    }
    public void setQueryKey(String queryKey) {
        this.queryKey = queryKey;
    }
    public String getQueryKey() {
        return this.queryKey;
    }

    public void setConstraints(HashMap<String, Object> constraints) {
        this.constraints = constraints;
    }
    public HashMap<String, Object> getConstraints() {
        return this.constraints;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }
    public List<String> getAttachments() {
        return this.attachments;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }
    public List<String> getOrder() {
        return this.order;
    }

    public void setAfter(int after) {
        this.after = after;
    }
    public int getAfter() {
        return this.after;
    }

    public void setBefore(int before) {
        this.before = before;
    }
    public int getBefore() {
        return this.before;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
    public int getLimit() {
        return this.limit;
    }
}
