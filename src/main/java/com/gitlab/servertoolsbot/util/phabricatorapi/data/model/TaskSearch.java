package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import java.util.Arrays;

public class TaskSearch extends ApplicationSearch {
    public TaskSearch() {
        this.setMethod("maniphest.search");
        this.setAttachments(Arrays.asList("projects", "subscribers", "columns"));
    }

    public Task perform() {
        return new Task(this.performRequest());
    }
}
