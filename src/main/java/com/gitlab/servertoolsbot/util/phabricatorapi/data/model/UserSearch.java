package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import java.util.Arrays;
import java.util.List;

public class UserSearch extends ApplicationSearch {
    public UserSearch() {
        this.setMethod("user.search");
        this.setAttachments(Arrays.asList("availability"));
    }

    public User perform() {
        return new User(this.performRequest());
    }

    @Override
    public String getMethod() {
        return this.method;
    }
    //public String getQueryKey() {
    //    return this.queryKey;
    //}
    //public HashMap<String, Object> getConstraints() {
    //    return constraints;
    //}
    public List<String> getAttachments() {
        return this.attachments;
    }
    //public List<String> getOrder() {
    //    return this.order;
    //}
    //public int getAfter() {
    //    return this.after;
    //}
    //public int getBefore() {
    //    return this.before;
    //}
    //public int getLimit() {
    //    return this.limit;
    //}
}
