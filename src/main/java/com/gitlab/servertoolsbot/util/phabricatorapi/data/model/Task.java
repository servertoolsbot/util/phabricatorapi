package com.gitlab.servertoolsbot.util.phabricatorapi.data.model;

import com.google.gson.JsonObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Task {
    public int id;
    public String type;
    public String phid;

    // Fields
    public String name;
    public String descriptionRaw;
    public String authorPHID;

    public User author() {
        HashMap<String, Object> constraints = new HashMap<>();
        constraints.put("phids", Arrays.asList(authorPHID));

        UserSearch search = new UserSearch();
        search.setConstraints(constraints);
        search.setLimit(1);

        return search.perform();
    }

    public String ownerPHID;

    public User owner() {
        HashMap<String, Object> constraints = new HashMap<>();
        constraints.put("phids", Arrays.asList(ownerPHID));

        UserSearch search = new UserSearch();
        search.setConstraints(constraints);
        search.setLimit(1);

        return search.perform();
    }

    public String closerPHID;

    public User closer() {
        HashMap<String, Object> constraints = new HashMap<>();
        constraints.put("phids", Arrays.asList(closerPHID));

        UserSearch search = new UserSearch();
        search.setConstraints(constraints);
        search.setLimit(1);

        return search.perform();
    }

    // Status
    public String statusValue;
    public String statusName;
    public String statusColorRaw;
    public Color statusColor;

    // Priority
    public int priorityValue;
    public String priorityName;
    public String priorityColorRaw;
    public Color priorityColor;

    public String points;
    public String subtype;
    public String spacePHID;

    // Dates
    public int dateClosed;
    public int dateCreated;
    public int dateModified;

    // Policy
    public String policyView;
    public String policyInteract;
    public String policyEdit;

    // Attachments

    // Subscribers
    public List<String> subscriberPHIDs = new ArrayList<>();

    public List<User> subscribers() {
        return subscriberPHIDs.stream().map(phid -> {
            HashMap<String, Object> constraints = new HashMap<>();
            constraints.put("phids", Arrays.asList(phid));

            UserSearch search = new UserSearch();
            search.setConstraints(constraints);
            search.setLimit(1);

            return search.perform();
        }).collect(Collectors.toList());
    }

    public int subscriberCount;
    public boolean viewerIsSubscribed;

    //TODO: Add attachments

    public Task(JsonObject obj) {
        obj = obj.get("result").getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject();
        JsonObject subObj = new JsonObject();
        JsonObject attachmentsObj = obj.get("attachments").getAsJsonObject();
        JsonObject subscribersObj = attachmentsObj.get("subscribers").getAsJsonObject();

        this.id = obj.get("id").getAsInt();
        this.type = obj.get("type").getAsString();
        this.phid = obj.get("phid").getAsString();

        // Fields
        obj = obj.get("fields").getAsJsonObject();

        this.name = obj.get("name").getAsString();
        this.descriptionRaw = obj.get("description").getAsJsonObject().get("raw").getAsString();
        this.authorPHID = obj.get("authorPHID").getAsString();
        if (!obj.get("ownerPHID").isJsonNull()) this.ownerPHID = obj.get("ownerPHID").getAsString();
        if (!obj.get("closerPHID").isJsonNull()) this.closerPHID = obj.get("closerPHID").getAsString();

        // Status
        subObj = obj.get("status").getAsJsonObject();

        this.statusValue = subObj.get("value").getAsString();
        this.statusName = subObj.get("name").getAsString();
        if (!subObj.get("color").isJsonNull()) {
            this.statusColorRaw = subObj.get("color").getAsString();
            this.statusColor = Color.getColor(this.statusColorRaw);
        }

        // Priority
        subObj = obj.get("priority").getAsJsonObject();

        this.priorityValue = subObj.get("value").getAsInt();
        this.priorityName = subObj.get("name").getAsString();
        this.priorityColorRaw = subObj.get("color").getAsString();
        this.priorityColor = Color.getColor(this.priorityColorRaw);


        if (!obj.get("points").isJsonNull()) this.points = obj.get("points").getAsString();
        this.subtype = obj.get("subtype").getAsString();
        if (!obj.get("spacePHID").isJsonNull()) this.spacePHID = obj.get("spacePHID").getAsString();

        // Dates
        if (!obj.get("dateClosed").isJsonNull()) this.dateClosed = obj.get("dateClosed").getAsInt();
        this.dateCreated = obj.get("dateCreated").getAsInt();
        this.dateModified = obj.get("dateModified").getAsInt();

        // Policy
        subObj = obj.get("policy").getAsJsonObject();

        this.policyView = subObj.get("view").getAsString();
        this.policyEdit = subObj.get("edit").getAsString();
        this.policyInteract = subObj.get("interact").getAsString();

        // Attachments

        this.subscriberCount = subscribersObj.get("subscriberCount").getAsInt();
        this.viewerIsSubscribed = subscribersObj.get("viewerIsSubscribed").getAsBoolean();
        if (this.subscriberCount != 0) {
            subscribersObj.get("subscriberPHIDs").getAsJsonArray().forEach(el -> this.subscriberPHIDs.add(el.getAsString()));
        }
    }
}
