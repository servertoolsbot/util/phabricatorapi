package com.gitlab.servertoolsbot.util.phabricatorapi.data.model.status;

import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.util.phabricatorapi.util.Web;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Statuses {
    public String defaultStatus;
    public String defaultClosedStatus;
    public String duplicateStatus;

    public List<String> openStatuses = new ArrayList<>();
    public List<String> closedStatuses = new ArrayList<>();
    public List<String> allStatuses = new ArrayList<>();
    public HashMap<String, String> statusMap = new HashMap<>();

    public Statuses(JsonObject obj) {
        obj = obj.get("result").getAsJsonObject();

        this.defaultStatus = obj.get("defaultStatus").getAsString();
        this.defaultClosedStatus = obj.get("defaultClosedStatus").getAsString();
        this.duplicateStatus = obj.get("duplicateStatus").getAsString();

        obj.get("openStatuses").getAsJsonArray().forEach(el -> this.openStatuses.add(el.getAsString()));
        this.closedStatuses = obj.get("closedStatuses").getAsJsonObject().entrySet().stream().map(Map.Entry::getValue).map(JsonElement::getAsString).collect(Collectors.toList());
        obj.get("allStatuses").getAsJsonArray().forEach(el -> this.allStatuses.add(el.getAsString()));
        obj.get("statusMap").getAsJsonObject().entrySet().forEach(entry -> statusMap.put(entry.getKey(), entry.getValue().getAsString()));
    }

    public static Statuses get() {
        return new Statuses(Web.postAndGetAsJson(PhabricatorApi.getPapi().getUrl("maniphest.querystatuses"), PhabricatorApi.getPapi().getParam()));
    }
}
